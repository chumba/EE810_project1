
import numpy as np
import matplotlib.pyplot as plt

with open ("traceFile.txt", "r") as myfile:
    data=myfile.read().replace('\n', '')

locs = [i for i in range(len(data)) if data.startswith('traceroute', i)]


with open('data.txt', 'w') as filen:

	for loc in locs:
		value = data[loc-14:loc].replace('ms','')
		value = value.replace('*','')
		value = value.replace(' ','')
		filen.write(value+"\n")

data = np.loadtxt('data.txt')

sorted_data = np.sort(data)

yvals=np.arange(len(sorted_data))/float(len(sorted_data))

plt.plot(sorted_data,yvals)

plt.show()

